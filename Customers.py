import random

class Customer:
    """
    Basic customer
    """
    def __init__(self, probability):
        self.customer_id = "CID" + str(random.getrandbits(24)) # it allows to generate pseudo random numbers with 24 bits in our case
        self.probability = probability
        self.budget = 0

    def get_drink(self, hour): # the function get_drink comes from the probability class and has the hour argument
        return self.probability.get_drink(hour)

    def get_food(self, hour):
        return self.probability.get_food(hour)

    def pay(self, drink, food, amount, time):
        self.budget -= amount

    def get_customer_id(self):
        return self.customer_id

    def get_budget(self):
        return self.budget


class CustomerRegularOnce(Customer): # it's a sub class
    """
    Regular once customer
    """
    def __init__(self, probability):
        super().__init__(probability) # the super() method refers to the fucntions coming from the main class
        self.budget = 100


class CustomerRegularTripadvisor(CustomerRegularOnce):
    """
    Regular customer by TripAdvisor
    """

    def _get_tip(self):
        return random.uniform(0, 1) * 10 #returns a floating random number

    def pay(self, drink, food, amount, time):
        super().pay(drink, food, amount, time)
        self.budget -= self._get_tip() #-= : substract and; for example: c -= c-a


class CustomerReturningRegular(Customer):
    """
    Returning customer (250+ budget)
    """
    def __init__(self, probability):
        super().__init__(probability)
        self.drink_history = {}
        self.food_history = {}
        self.budget = 250

    def pay(self, drink, food, amount, time):
        super().pay(drink, food, amount, time) #the self () method allows to call a function belonging to a mother class
        self.drink_history.update({
            time: drink,
        })
        self.food_history.update({
            time: food,
        })


class CustomerReturningHipster(CustomerReturningRegular):
    """
    Returning customer (500+ budget)
    """
    def __init__(self, probability):
        super().__init__(probability)
        self.budget = 500
