import csv
import os
from datetime import datetime

def main():
    # Specify the path to reach the CSV file
    file_path = "/Users/Alodie/PycharmProjects/examunamur2018-data/Data/Coffeebar_2013-2017.csv"
    # Check if the file really exists (raise an exception if not)
    if not os.path.exists(file_path):
        message = "The CSV file doesn't exist!"
        raise Exception(message)
    informations = _read_file(file_path) #It allows us to get the information from the function _read_file()
    #  Now we read the entire file and have every data in memory (into informations)

    # Treat info :
    # Save every foods (without duplicates)
    foods = set([l.get('food', False) for l in informations if l.get('food', False)])
    # Save every drinks (without duplicates)
    drinks = set([l.get('drinks', False) for l in informations if l.get('drinks', False)])
    # Nb of customers (without duplicates)
    customers = len(set([l.get('customer', False) for l in informations if l.get('customer', False)]))
    # We have every foods/drinks into a list; we can use these info to count how many time they has been sell
    nb_foods = dict.fromkeys(foods, 0)
    nb_drinks = dict.fromkeys(drinks, 0)
    for food in foods:
        nb_foods.update({
            food: sum([1 for l in informations if l.get('food', False) == food]),
        })
    for drink in drinks:
        nb_drinks.update({
            drink: sum([1 for l in informations if l.get('drinks', False) == drink]),
        })

    stat_hours = get_buy_stats(informations)

    # Display part
    print("\n\n########## Part 1") # \n means that we want to go to the next line
    print("Number of customers: %s" % customers)
    print("Foods:\n- %s" % "\n- ".join(foods)) # The join method join() returns a string in which the string elements of sequence have been joined by str separator
    print("Drinks:\n- %s" % "\n- ".join(drinks))
    print("\n\n########## Part 2")
    print("Amount of sold foods:")
    for food, total in nb_foods.items(): # The method items() returns a list of dictionary(key, value)
        print("- %s %s" % (food, total)) # We have two strings : food and total
    print("Amount of sold drinks:")
    for drink, total in nb_drinks.items():
        print("- %s %s" % (drink, total))
    print("\n\n########## Part 3")
    for hour, stat in stat_hours.items():
        print("%s" % hour)
        total = float(stat.pop('Total', 0)) # Float is used to get real values of percentages
        for name, nb in stat.items():
            print("\t%s: %.2f%%" % (name, nb/total*100)) # %.2f means that we want only 2 digits after the point and that we want a floating value
    return 0 # End of the main function

    # Probabilities part

def get_buy_stats(informations):
    hour_format = "%H:%M"
    stat_hours = {}
    for line in informations:
        hour = line.get('time', False).strftime(hour_format)
        actual = stat_hours.get(hour, {})
        # Increment food
        food = line.get('food', False) or 'No food' # Return 'food' or otherwise return 'no food' when it's false (there is no sold food)
        food_actual = actual.get(food, 0) + 1
        # Increment drink
        drink = line.get('drinks', False) or 'No drink'
        drink_actual = actual.get(drink, 0) + 1
        total = actual.get('Total', 0) + 1
        actual.update({
            'Total': total,
            drink: drink_actual,
            food: food_actual,
        })
        stat_hours.update({
            hour: actual,
        })
    return stat_hours

def _read_file(file_path):

    # Date format from the file
    date_format = "%Y-%m-%d %H:%M:%S"  # Y has to be in capital letter because there are 4 numbers and not 2
    informations = [] #We create a empty list where we will put all the information of the file
    with open(file_path, "r") as csv_file: #We use 'r' to read the file
        reader = csv.DictReader(csv_file, delimiter=";")
        for line in reader:
            time = datetime.strptime(line.get('TIME', ''), date_format) #line.get means that I want to read the file line by line
            customer = line.get('CUSTOMER', '') # The first value is for the column's name and the second ('') is the returned value if there is no value for this line
            drinks = line.get('DRINKS', '')
            food = line.get('FOOD', '')
            informations.append({ #I want to append each line to the list
                'time': time,
                'customer': customer,
                'drinks': drinks,
                'food': food,
            })
        return informations

if __name__ == "__main__": # Pycharm reads first the main function and then goes through the other functions after
    main()