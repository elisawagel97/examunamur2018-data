from datetime import datetime
import csv
import random

class Probability:# We build the class instance and load the file info
    def __init__(self):

        self._file_informations = []
        self._stat_hour_foods = {}
        self._stat_hour_drinks = {}
        self.force_reload_file()

    def _get_probabilities_from_file(self):

        # We load drinks and foods probabilities (by timestep) based on the CSV file
        # Build dict of drinks (stat_hour_drinks):
        # Key = Hour; Value = dict with: key = drink name; value = number
        # Build dict of foods (stat_hour_foods):
        # Key = Hour; Value = dict with: key = food name; value = number

        self._read_file()
        informations = self._file_informations
        hour_format = "%H:%M"
        stat_hour_drinks = {}
        stat_hour_foods = {}
        for line in informations:
            hour = line.get('time', False).strftime(hour_format)
            actual_drinks = stat_hour_drinks.get(hour, {})
            actual_foods = stat_hour_foods.get(hour, {})
            # Increment food
            food = line.get('food', False) or 'No food'
            food_actual = actual_foods.get(food, 0) + 1
            # Increment drink
            drink = line.get('drinks', False) or 'No drink'
            drink_actual = actual_drinks.get(drink, 0) + 1
            # Update number into dicts
            actual_drinks.update({
                drink: drink_actual,
            })
            actual_foods.update({
                food: food_actual,
            })
            # Then update the "sub" dict into "main" dict
            stat_hour_drinks.update({
                hour: actual_drinks,
            })
            stat_hour_foods.update({
                hour: actual_foods,
            })
        self._stat_hour_foods = stat_hour_foods
        self._stat_hour_drinks = stat_hour_drinks
        return True # return bool

    def _read_file(self): #Load the file and save it into a dict

        file_path = "/Users/Alodie/PycharmProjects/examunamur2018-data/Data/Coffeebar_2013-2017.csv"

        date_format = "%Y-%m-%d %H:%M:%S" # Date format from the file

        # Read informations
        informations = []
        with open(file_path, "r") as csv_file:
            reader = csv.DictReader(csv_file, delimiter=";")
            for line in reader:
                time = datetime.strptime(line.get('TIME', ''), date_format)
                customer = line.get('CUSTOMER', '')
                drinks = line.get('DRINKS', '')
                food = line.get('FOOD', '')
                informations.append({
                    'time': time,
                    'customer': customer,
                    'drinks': drinks,
                    'food': food,
                })
        self._file_informations = informations
        return True # return bool

    def force_reload_file(self):  # Force reload file data (in case of the file has been updated)

        self._get_probabilities_from_file()
        return True  # return bool

    def get_drink(self, hour): #Get a drink using the hour argument; based on the probability rule loaded (the file)

        stat_hours = self._stat_hour_drinks
        stat = stat_hours.get(hour, {}) #We use {} because we don't want an integer value but a dictionnary if "hour" isn't in "stat hours"
        values = stat.values() # # The method values() returns a list of all the values available in a given dictionary
        total = sum([nb for nb in values])
        probs = {}
        drinks = {}
        cpt = 1 #We made a dictionnary where the key is a number and the value is a drink
        for name, nb in stat.items(): #the item() methods returns the values and the key of a dictionnary
            if name not in drinks.keys(): # The method keys() returns a list of all the available keys in the dictionary
                drinks.update({ # The method update() adds to the dictionary news values from a new dictionary
                    cpt: name,
                })
                cpt += 1
            prob = 0
            if total:
                prob = nb/float(total)
            probs.update({
                name: prob,
            })
        result = random.choices(list(drinks.keys()), list(probs.values()))[0] # The list function returns a mutable sequence lists of elements
        #The function random.choices() gets 2 arguments (a list of numbers and probabilities)
        #Result gives us a list of numbers but we only need the first one so we set [0]
        return drinks.get(result, '') # return string

    def get_food(self, hour): #Get a food using the hour argument; based on the probability rule loaded

        stat_hours = self._stat_hour_foods # the dictionnary stat_hour_food is linked to the main class with the self method
        stat = stat_hours.get(hour, {})
        values = stat.values()
        total = sum([nb for nb in values])
        probs = {}
        foods = {}
        cpt = 1
        for name, nb in stat.items():
            if name not in foods.keys():
                foods.update({
                    cpt: name,
                })
                cpt += 1
            prob = 0
            if total:
                prob = nb / float(total)
            probs.update({
                name: prob,
            })
            result = ''
            if probs:
                result = random.choices(list(foods.keys()), list(probs.values()))[0]
            return foods.get(result, 'No food') # return string