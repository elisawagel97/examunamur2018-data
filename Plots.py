import matplotlib.pyplot as plt

# Repartion of sold drinks

x = ['water', 'coffee', 'milkshake', 'tea', 'frappucino', 'soda']
y = [40915, 53833, 40752, 40556, 40436, 95583]

plt.bar(x, y, color='r')
plt.xlabel('Drinks')
plt.ylabel('Total amount of sold drinks')

plt.title('Drinks sold repartition over 5 years')

plt.show()

# Repartion of sold foods

x = ['sandwich', 'muffin', 'pie', 'cookie']
y = [68623, 31785, 32010, 31852]

plt.bar(x, y, color='y')
plt.xlabel('Foods')
plt.ylabel('Total amount of sold foods')

plt.title('Foods sold repartition over 5 years')

plt.show()
