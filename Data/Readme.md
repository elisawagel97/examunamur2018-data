1. What does your project do? 

Our project aims to helps a coffee bar to better handle its business and how to adapt its strategy after analysing a data set from a customers' CSV file. 

2. How is it set up?

If someone were to pick up our codebase, he has to first paste his file's path in three files : Exploratory (line 7), Probabilities (line 57) and Simulations (line 74). Then, he can run the python files in the following order: Exploratory, Plots, Probabilities, Customers and finally Simulations. However, in the Plots part you should select one plot at a time and execute this selection in console.

3. How is it used? 

Once it's up and running you get different results in the Exploratory and Simulations parts. In the Exploratory part you receive the amount of unique customers, the amount of sold foods and drinks, the different kind of foods and drinks. Finally you get for five years the proportions of sold foods and drinks at each specific time in a day as wel as the plots of these proportions. In the Simulations part you a new CSV file called generation.csv with simulated purchases of foods and drinks. 

4. Do you have safety net?

We implemented an error message : "The CSV file doesn't exist!" to indicate if there is any problem which concerns the file path.

5. License information

We used the 3.6 version of Python. 