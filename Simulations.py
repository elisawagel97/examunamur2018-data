import csv
from datetime import datetime
import random
from Customers import CustomerRegularOnce # we import different classes from our previous python file
from Customers import CustomerRegularTripadvisor
from Customers import CustomerReturningRegular
from Customers import CustomerReturningHipster
from Probabilities import Probability

returning_customers = []
date_format = "%Y-%m-%d %H:%M:%S"

def generate_returning_customers(probability): # we define the function generate_returning_customer
    customer_type = {
        1: 2/3,  # CustomerReturningRegular
        2: 1/3,  # CustomerReturningHipster
    }

    customer_values = {
        1: CustomerReturningRegular,
        2: CustomerReturningHipster,
    }
    for x in range(0, 1000): # we make a loop which will go through the 1000 returning customers
        number = random.choices(list(customer_type.keys()), list(customer_type.values()))[0]
        customer = customer_values.get(number)(probability)
        returning_customers.append(customer) #we add the retuning_customers list  the customer dictionnary


def generate_customer(probability):
    # Statistiques:
    # CustomerRegularOnce => 80% * 9/10 = 72%
    # CustomerRegularTripadvisor => 80% * 1/10 = 8%
    # CustomerReturningRegular => 20% * 2/3 = 13.3333%
    # CustomerReturningHipster => 20% * 1/3 = 6.66666%

    customer_type = {
        1: 0.72,  # CustomerRegularOnce
        2: 0.08,  # CustomerRegularTripadvisor
        3: 0.1333333333,  # CustomerReturningRegular
        4: 0.0666666666,  # CustomerReturningHipster
    }

    customer_values = { # we create a dictionary with different keys referencing different type of customers
        1: CustomerRegularOnce,
        2: CustomerRegularTripadvisor,
        3: CustomerReturningRegular,
        4: CustomerReturningHipster,
    }
    number = random.choices(list(customer_type.keys()), list(customer_type.values()))[0] #with the random method we associate a key (from the dictionnary) with a probability
    CustomerClass = customer_values.get(number) # we create a class which is a dictionnary to whom we added a number to the dictionnary customer_value. Based on this number, we can directly get the right class that we want to instantiate
    if isinstance(CustomerClass, CustomerReturningRegular): # If isinstance check from which class the object is , in our case from which type of customer
        customer = False
        max_size = len(returning_customers)
        while not customer:
            index = int(random.uniform(0, max_size - 1)) #The random.uniform() function return a float value and the int behind allows to only get an integer
            customer_value = returning_customers[index] #in the list returning customer we get the customer which is in the position indicated by the index
            if customer_value.budget > 0:# if the customer has enough money, then we keep it, otherwise we eject him from the list.
                customer = customer_value
    else:
        customer = CustomerClass(probability)
    return customer


def remove_returning_customer(customer): # this part is used to remove some returning customers
    cpt = 0 # we start at the point 0
    for returning_customer in returning_customers:# we loop all the returning customers to find the one who was given in parameters for this function
        if returning_customer == customer:
            returning_customers.pop(cpt)
            break # when we find this customer we stop the loop
        cpt += 1 # the cpt is increased by 1 at each time we go through the loop and it is used to memorize the position of the customer we need to cancel in the list


def get_times():
    filepath = "/Users/Alodie/PycharmProjects/examunamur2018-data/Data/Coffeebar_2013-2017.csv"
    # Read informations
    with open(filepath, "r") as csv_file:
        reader = csv.DictReader(csv_file, delimiter=";")
        times = [datetime.strptime(l.get('TIME', ''), date_format) for l in reader]
    return times


if __name__ == "__main__":
    hour = '13:16' # at a certain specific time
    probability = Probability()
    food_prices = { # we create a dictionary with the keys (type of food) associated to a value (price)
        'sandwich': 5,
        'cookie': 2,
    }
    drink_prices = {
        'milkshake': 5,
        'frappucino': 4,
        'water': 2,
    }
    expensive = max(food_prices.values()) + max(drink_prices.values()) # the dictionary is composed by the maximal value of the dictionnaries : food_prices and drink_prices
    generate_returning_customers(probability)
    times = get_times() # we returns the numeric value related to a specific time for the date we decided
    hour_format = "%H:%M"
    filepath = "generation.csv"
    headers = ["TIME", "CUSTOMER", "DRINKS", "FOOD"]
    with open(filepath, "w") as csv_file:
        writer = csv.DictWriter(csv_file, fieldnames=headers)
        writer.writeheader() # it allows us to write a csv file
        for time in times: # we create a loop which will go though each timeslot of the file newly created and will associate for each specific time a customer ID, a sold drink and sometimes a sold food
            hour = time.strftime(hour_format)
            str_time = time.strftime(date_format)
            customer = generate_customer(probability)
            food = customer.get_food(hour)
            drink = customer.get_drink(hour)
            food_price = 0
            if food not in ['', False, 'No food']:
                food_price = food_prices.get(food, 3) # If food isn't a sandwich or a cookie then the price is 3
            drink_price = 0
            if drink not in ['', False, 'No drink']:
                drink_price = drink_prices.get(drink, 3) # If drink isn't a milkshake, a frappucino or water  then the price is 3
            amount = food_price + drink_price # this dictionnary can be seen as a constraint that will or not lead to the ejection of a returning
            customer.pay(drink, food, amount, str_time) # this function has different arguments
            if customer.get_budget() < 5 + 5: # a customer who can't buy the most expensive product has to be removed from the returning customer list,in our case the higher price for food is 5 and for drinks it's 5 as well
            # The function get.budget has been definec in the Customer class from the Customers file
                remove_returning_customer(customer)
            values = {
                'TIME': str_time,
                'CUSTOMER': customer.get_customer_id(),
                'DRINKS': drink,
                'FOOD': food,
            }
            writer.writerow(values) # write the different value in row so that we create a new csv file with new cutomers